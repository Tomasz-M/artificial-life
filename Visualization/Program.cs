﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using Brush = System.Drawing.Brush;
using Brushes = System.Drawing.Brushes;
using Color = System.Drawing.Color;
using Pen = System.Drawing.Pen;

namespace Visualization
{
    class Program
    {
        //fields
        static double pi180 = Math.PI  / 180.0;
        static bool MN = false;
        static bool NN = true;
        static string file = @"63144.temptxt";
        static void Main(string[] args)
        {
            int swarmSize = 1 + 100;
            int size = 512;
            Bitmap bitmap = new Bitmap(size, size);
            Graphics g = Graphics.FromImage(bitmap);
            g.Clear(Color.Beige);
            Brush pen = new SolidBrush(Color.MediumSeaGreen);
            Brush penPred = new SolidBrush(Color.OrangeRed);
            Brush penPredConf = new SolidBrush(Color.Violet);
            Pen pen2 = new Pen(Color.Black,1);
            string path = @"D:\Nowy folder\simu\LSTM\visual\" + file;
            if (NN)
                path = @"D:\Nowy folder\simu\NN\visual\" + file;
            else if (NN)
                path = @"D:\Nowy folder\simu\MN\visual\" + file;
            List<Bitmap> frames = new List<Bitmap>();
            int counter = 0;
            Rectangle rectf = new Rectangle(5,5,100,100);
            System.Windows.Media.Imaging.GifBitmapEncoder gEnc = new GifBitmapEncoder();
            IntPtr bmp = bitmap.GetHbitmap();

            using (StreamReader sr = new StreamReader(path))
            {
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    counter++;
                    int numberOfAlive = -1;
                    string[] data = line.Split(';');
                    for (int i = 0; i < data.Length / 4; i++)
                    {
                        int isalive = int.Parse(data[4 * i]);
                        if (isalive == 1)
                        {
                            numberOfAlive++;
                            float x = size / 2 + float.Parse(data[4 * i + 1]),
                                y = size / 2 + float.Parse(data[4 * i + 2]);
                            double angle = double.Parse(data[4 * i + 3]);
                            float x2 = (float) (x + (Math.Cos(angle * pi180) * 3));
                            float y2 = (float) (y + Math.Sin(angle * pi180) * 3);
                            if (i % swarmSize == 0)
                            {
                                g.FillEllipse(int.Parse(data[data.Length-1])==1?penPred : penPredConf, x - 3, y - 3, 6, 6);
                            }
                            else
                            {
                                g.FillEllipse(pen, x - 3, y - 3, 6, 6);
                            }
                            g.DrawLine(pen2, x, y, x2, y2);
                            if (numberOfAlive == 0)
                                g.DrawEllipse(pen2, x - 200, y - 200, 400, 400);
                        }
                    }
                    g.DrawString(numberOfAlive.ToString(), new Font("Tahoma", 8), Brushes.Black, rectf);
                    bmp = bitmap.GetHbitmap();
                    //frames.Add(new Bitmap(bitmap));
                    BitmapSource src = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                        bmp,
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                    gEnc.Frames.Add(BitmapFrame.Create(src));
                    g.Clear(Color.Beige);
                    if (counter % 100 == 0)
                    {
                        if (MN)
                            SaveGif(gEnc, @"D:\Nowy folder\simu\Visualisation\MN\1\" + counter + ".gif");
                        else if (NN)
                            SaveGif(gEnc, @"D:\Nowy folder\simu\Visualisation\NN\1\" + counter + ".gif");
                        else
                            SaveGif(gEnc, @"D:\Nowy folder\simu\Visualisation\LSTM\1\" + counter + ".gif");
                        GC.Collect();
                        gEnc = new GifBitmapEncoder();
                    }
                }
            }

            Console.WriteLine("VISU - DONE");
            Console.ReadLine();
        }

        private static void SaveGif(GifBitmapEncoder gEnc, string s)
        {
            //System.Windows.Media.Imaging.GifBitmapEncoder gEnc = new GifBitmapEncoder();

            //foreach (System.Drawing.Bitmap bmpImage in frames)
            //{
            //    IntPtr bmp = bmpImage.GetHbitmap();
            //    var src = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
            //        bmp,
            //        IntPtr.Zero,
            //        Int32Rect.Empty,
            //        BitmapSizeOptions.FromEmptyOptions());
            //    gEnc.Frames.Add(BitmapFrame.Create(src));
            //    //DeleteObject(bmp); // recommended, handle memory leak
            //}
            //frames.Clear();
            using (FileStream fs = new FileStream(s, FileMode.Create))
            {
                gEnc.Save(fs);
                fs.Close();
            }
        }
    }
}
