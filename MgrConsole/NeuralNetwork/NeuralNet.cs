﻿using System.Collections.Generic;

namespace MgrConsole.NeuralNetwork
{
    public class NeuralNet : IController
    {
        private NeuralNeuron[] HiddenNeurons { get; set; }
        private NeuralNeuron[] OutpuNeurons { get; set; }
        private int _resultSize;

        public NeuralNet(List<byte> genome, int numberOfHiddenNeurons, int numberOfOutputNeurons, int numberOfStates) //TODO TEMP!!!
        {
            HiddenNeurons = new NeuralNeuron[numberOfHiddenNeurons];
            OutpuNeurons = new NeuralNeuron[numberOfOutputNeurons];
            int size = numberOfStates + 1; //bias
            int courentGenomeIndex = 0;
            for (int i = 0; i < numberOfHiddenNeurons; i++)
            {
                HiddenNeurons[i] = new NeuralNeuron(genome, courentGenomeIndex, size);
                courentGenomeIndex += size;
            }

            size = numberOfHiddenNeurons + 1; //bias
            for (int i = 0; i < numberOfOutputNeurons; i++)
            {
                OutpuNeurons[i] = new NeuralNeuron(genome, courentGenomeIndex, size);
                courentGenomeIndex += size;
            }

            _resultSize = HiddenNeurons.Length + OutpuNeurons.Length;
        }

        public int[] SendForward(int[] states, int firsIndex = 0)
        {
            int[] resultTable = new int[_resultSize];
            for (int i = 0; i < HiddenNeurons.Length; i++)
            {
                NeuralNeuron neuron = HiddenNeurons[i];
                for (int j = 0; j < neuron.Count - 1; j++)
                {
                    neuron.NetValue += neuron[j] * states[firsIndex + j];
                }
                neuron.NetValue += neuron[neuron.Count - 1]; //BIAS
            }
            SetHiddenOutputsIntoResultTable(resultTable);
            ClearNeuronsValues(HiddenNeurons);
            for (int i = 0; i < OutpuNeurons.Length; i++)
            {
                NeuralNeuron neuron = OutpuNeurons[i];
                for (int j = 0; j < HiddenNeurons.Length; j++)
                {
                    neuron.NetValue += neuron[j] * resultTable[j];
                }
                neuron.NetValue += neuron[HiddenNeurons.Length];
                resultTable[HiddenNeurons.Length + i] = neuron.OutputValue;
            }
            ClearNeuronsValues(OutpuNeurons);
            return resultTable;
        }

        private void ClearNeuronsValues(NeuralNeuron[] neurons)
        {
            foreach (NeuralNeuron neuron in neurons)
                neuron.NetValue = 0;
        }

        private void SetHiddenOutputsIntoResultTable(int[] resultTable)
        {
            for (var i = 0; i < HiddenNeurons.Length; i++)
            {
                resultTable[i] = HiddenNeurons[i].OutputValue;
            }
        }

        class NeuralNeuron
        {
            public float NetValue { get; set; }

            public int OutputValue =>
                NetValue > 0 ? 1 : 0;

            public float[] Weigths { get; set; }
            public int Count => Weigths.Length;

            public NeuralNeuron(List<byte> genome, int startIndex, int size)
            {
                Weigths = new float[size];
                for (var i = 0; i < size; i++)
                {
                    Weigths[i] = ((2 * (genome[startIndex + i] / 256f)) - 1);
                }
            }

            public float this[int i]
            {
                get { return Weigths[i]; }
                set { Weigths[i] = value; }
            }
        }
    }
}
