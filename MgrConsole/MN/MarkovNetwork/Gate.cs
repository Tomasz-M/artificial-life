﻿using System;
using System.Collections.Generic;

namespace MgrConsole.MN.MarkovNetwork
{
    public class Gate
    {
        public List<int> Inputs { get; set; }
        public List<int> Outputs { get; set; }
        public byte[,] Probabilities { get; set; }
        public int[] SumOfProbabilities { get; set; }
        private int numberOfProbabilitiesInputs = 0;
        private int numberOfProbabilitiesOutputs = 0;

        public Gate(int numberOfGateInputs, int numberOfGateOutputs, int numberOfNodes, int minIndexOfOutput)
        {
            minIndexOfOutput = 0;
            Inputs = new List<int>();
            Outputs = new List<int>();
            numberOfProbabilitiesInputs = (int)Math.Pow(2, numberOfGateInputs);
            numberOfProbabilitiesOutputs = (int)Math.Pow(2, numberOfGateOutputs);
            Probabilities = new byte[numberOfProbabilitiesInputs, numberOfProbabilitiesOutputs];
            SumOfProbabilities = new int[numberOfProbabilitiesInputs];
            while (Inputs.Count < numberOfGateInputs)
            {
                int toAdd = StaticFields.RandomGenerator.Next(0, numberOfNodes);
                if (Inputs.Contains(toAdd))
                    continue;

                Inputs.Add(toAdd);
            }

            while (Outputs.Count < numberOfGateOutputs)
            {
                int toAdd = StaticFields.RandomGenerator.Next(minIndexOfOutput, numberOfNodes);
                if (Outputs.Contains(toAdd))
                    continue;

                Outputs.Add(toAdd);
            }
            for (int i = 0; i < numberOfProbabilitiesInputs; i++)
            {
                for (int j = 0; j < numberOfProbabilitiesOutputs; j++)
                {
                    byte temp = StaticFields.RandomByte();
                    Probabilities[i, j] = temp;
                    SumOfProbabilities[i] += temp;
                }
            }
        }

        public Gate(List<byte> genome, int numberOfNodes, int minIndexOfOutput, ref int currentIndex)
        {
            minIndexOfOutput = 0;
            Inputs = new List<int>();
            //INPUTS
            int size = 1 + genome[currentIndex] % StaticFields._maxNumberOfInputs; //numberOfInputs
            currentIndex++;
            for (int i = 0; i < size; i++)
            {
                Inputs.Add(genome[currentIndex] % numberOfNodes);
                currentIndex++;
            }
            //OUTPUTS
            Outputs = new List<int>();
            size = 1 + genome[currentIndex] % StaticFields._maxNumberOfOutputs; //numberOfOutputs
            currentIndex++;
            for (int i = 0; i < size; i++)
            {
             
                Outputs.Add(minIndexOfOutput + genome[currentIndex] % (numberOfNodes - minIndexOfOutput));
                currentIndex++;
            }
            //PROBABILITIES
            int probSizeOut = (int)Math.Pow(2, Outputs.Count), probSizeInt = (int)Math.Pow(2, Inputs.Count); 
            Probabilities = new byte[probSizeInt, probSizeOut];
            SumOfProbabilities = new int[probSizeInt];
            for (int i = 0; i < probSizeInt; i++)
            {
                for (int j = 0; j < probSizeOut; j++)
                {
                    Probabilities[i, j] = genome[currentIndex];
                    SumOfProbabilities[i] += genome[currentIndex];
                    currentIndex++;
                }
            }

        }

        public List<int> Compute(int[] states, int startIndex = 0)
        {
            int indexOfInputState = GetIndexOfInputState(states, startIndex);
            List<int> indexesToSetOn = GetindexOfOutputsToLit(indexOfInputState);

            return indexesToSetOn;
        }

        private List<int> GetindexOfOutputsToLit(int indexOfInputState)
        {
            List<int> indexesToSetOn = new List<int>();

            int j = 0;
            int rand = StaticFields.RandomGenerator.Next(0, SumOfProbabilities[indexOfInputState]);
            while (rand > Probabilities[indexOfInputState,j])
            {
                rand -= Probabilities[indexOfInputState, j];
                j++;
            }

            for (int i = 0; i < Outputs.Count; ++i)
            {
                if(((j >> i) & 1) == 1)
                    indexesToSetOn.Add(Outputs[i]);
            }

            return indexesToSetOn;
        }
        
        private int GetIndexOfInputState(int[] states, int startIndex)
        {
            int inputsSize = Inputs.Count;
            int indexOfInputState = 0;
            for (int i = 0; i < inputsSize; i++)
            {
                indexOfInputState = (indexOfInputState << 1) + ((states[startIndex + Inputs[i]]) & 1);
            }
            return indexOfInputState;
        }

        public override string ToString()
        {
            string result = "{(";
            int i = 0;
            while (i < Inputs.Count - 1)
            {
                result += Inputs[i].ToString() + ",";
                i++;
            }
            result += Inputs[i].ToString() + ") - (";

            i = 0;
            while (i < Outputs.Count - 1)
            {
                result += Outputs[i].ToString() + ",";
                i++;
            }
            result += Outputs[i].ToString() + ")}";
            return result;
        }
    }
}
