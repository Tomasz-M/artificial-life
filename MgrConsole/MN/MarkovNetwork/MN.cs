﻿namespace MgrConsole.MN.MarkovNetwork
{
    //Markov Netowrk
    public class MN : IController
    {
        public Gate[] Gates { get; set; }

        public MN(int numberOfGates, int numberOfGateInputs, int numberOfGateOutputs, int numberOfNodes, int minIndexOfOutput)
        {
            Gates = new Gate[numberOfGates];
            for (int i = 0; i < numberOfGates; i++)
                Gates[i] = new Gate(numberOfGateInputs, numberOfGateOutputs, numberOfNodes, minIndexOfOutput);
        }

        public MN(int numberOfGates)
        {
            Gates = new Gate[numberOfGates];
        }

        public override string ToString()
        {
            string result = "";
            for (var i = 0; i < Gates.Length-1; i++)
            {
                result += Gates[i].ToString() + "  |  ";
            }
            result += Gates[Gates.Length - 1].ToString();
            return result;
        }
    }
}
