﻿using System;
using System.Collections.Generic;
using System.Linq;
using MgrConsole.MN.MarkovNetwork;
using MgrConsole.NeuralNetwork;

namespace MgrConsole.MN.Agents
{
    class PhenotypeMN : IPhenotype
    {
        public int NumberOfOutputNeurons { get; set; }
        public int NumberOfHiddenNeurons { get; set; }
        public int Size { get; set; }
        public int _currentNumberOfGates;
        public int _currentNumberOfInputs = 4;
        public int _currentNumberOfOutputs = 4;
        public int MaxNodes { get; set; }
        public int NumberOfSensorsLayers { get; set; }
        public int Sensors { get; set; }
        public int Range { get; set; }
        public int Id { get; set; }

        public int ParrentId { get; set; }
        public int MasterId { get; set; }

        public List<byte> Genome { get; set; }
        public double Fitness { get; set; }

        //numberOfSensorsLayers - 1 for predator, 2 for prey
        public PhenotypeMN(int maxNodes, int numberOfSensorsLayers = 1, int sensors = 12)
        {
            _currentNumberOfGates = StaticFields._maxNumberOfGates;
            Fitness = 0;
            Sensors = sensors;
            MaxNodes = maxNodes;
            NumberOfSensorsLayers = numberOfSensorsLayers;
            Range = 180;
            Size = _currentNumberOfGates * (2 /*Nin and Nout*/ + _currentNumberOfInputs /*inputs indexes*/ +
                                            _currentNumberOfOutputs /*outputindexes*/ +
                                            (int) Math.Pow(2, _currentNumberOfInputs) * (int) Math.Pow(2, _currentNumberOfOutputs));
            Genome = CreateGenome();
            ParrentId = -1;
            Id = StaticFields._IdCounter++;
            MasterId = Id;
        }

        private PhenotypeMN(PhenotypeMN parent)
        {
            _currentNumberOfGates = parent._currentNumberOfGates;
            MaxNodes = parent.MaxNodes;
            NumberOfSensorsLayers = parent.NumberOfSensorsLayers;
            Sensors = parent.Sensors;
            Range = parent.Range;
            Genome = CopyGenome(parent.Genome);
            ParrentId = parent.Id;
            MasterId = parent.MasterId;
            Id = StaticFields._IdCounter++;
            Size = parent.Size;
        }

        public PhenotypeMN(string[] strings)
        {
            Id = Convert.ToInt32(strings[1]);
            ParrentId = Convert.ToInt32(strings[2]);
            MaxNodes = Convert.ToInt32(strings[3]);
            NumberOfSensorsLayers = Convert.ToInt32(strings[4]);
            Sensors = Convert.ToInt32(strings[5]);
            Range = Convert.ToInt32(strings[6]);
            int currentGenomeSize = Convert.ToInt32(strings[7]);

            Genome = new List<byte>();
            int indexOfLastGenome = 8 + currentGenomeSize;
            for (int i = 8; i < indexOfLastGenome; i++)
            {
                Genome.Add(Convert.ToByte(strings[i]));
            }

            if (strings.Length > indexOfLastGenome)
                _currentNumberOfGates = Convert.ToByte(strings[indexOfLastGenome++]);
            if (strings.Length > indexOfLastGenome)
                MasterId = strings[indexOfLastGenome].Equals("") ? Id : Convert.ToInt32(strings[indexOfLastGenome]);
            else
                MasterId = Id;
            Size = _currentNumberOfGates * (2 /*Nin and Nout*/ + _currentNumberOfInputs /*inputs indexes*/ +
                                            _currentNumberOfOutputs /*outputindexes*/ +
                                            (int)Math.Pow(2, _currentNumberOfInputs) * (int)Math.Pow(2, _currentNumberOfOutputs));
            for (int i = currentGenomeSize; i < Size; i++)
            {
                Genome.Add(StaticFields.RandomByte());
            }

        }

        private List<byte> CopyGenome(List<byte> parentGenome)
        {
            List<byte> newGenome = new List<byte>();
            foreach (byte b in parentGenome)
            {
                newGenome.Add(b);
            }
            return newGenome;
        }


        public IController GetController()
        {
            return GetMarkovNetwork();
        }
        public MarkovNetwork.MN GetMarkovNetwork()
        {
            MarkovNetwork.MN MarkovNetwork = new MarkovNetwork.MN(_currentNumberOfGates);
            int startIndex = 0;
            for (int i = 0; i < _currentNumberOfGates; i++)
            {
                MarkovNetwork.Gates[i] = new Gate(Genome, MaxNodes, NumberOfSensorsLayers * Sensors,
                    ref startIndex);
            }
            return MarkovNetwork;
        }

        private List<byte> CreateGenome()
        {
            byte[] randomBytes = new byte[Size];
            StaticFields.RandomGenerator.NextBytes(randomBytes);
            return randomBytes.ToList();
        }

        private List<byte> CreateGate()
        {
            byte[] randomBytes = new byte[Size];
            StaticFields.RandomGenerator.NextBytes(randomBytes);
            return randomBytes.ToList();
        }
        
        public void GeneMutatation(double mutationProbability = 0.01)
        {
            for (int i = 0; i < Genome.Count; i++)
            {
                if (StaticFields.RandomGenerator.NextDouble() < mutationProbability)
                    Genome[i] = StaticFields.RandomByte();
            }
        }
        
        public void GeneDuplication(double duplicationProbability = 0.05)
        {
            if (StaticFields.RandomGenerator.NextDouble() < duplicationProbability)
            {
                int startPoint = StaticFields.RandomGenerator.Next(Genome.Count);
                int endPoint = StaticFields.RandomGenerator.Next(Genome.Count);
                
                if (startPoint > endPoint)
                {
                    int temp = endPoint;
                    endPoint = startPoint;
                    startPoint = temp;
                }

                List<byte> bytesToDuplicate = Genome.GetRange(startPoint, 1 + endPoint - startPoint);
                int startInsertPoint = StaticFields.RandomGenerator.Next(Genome.Count - bytesToDuplicate.Count);
                int currentIndex = 0;

                for (int i = startInsertPoint; i < bytesToDuplicate.Count; i++)
                {
                    Genome[i] = bytesToDuplicate[currentIndex];
                    currentIndex++;
                }
            }
        }

        public void GeneDeletion(double deletionProbability = 0.02)
        {
            if (StaticFields.RandomGenerator.NextDouble() < deletionProbability)
            {
                int startPoint = StaticFields.RandomGenerator.Next(Genome.Count);
                int endPoint = StaticFields.RandomGenerator.Next(Genome.Count);
                
                if (startPoint > endPoint)
                {
                    int temp = endPoint;
                    endPoint = startPoint;
                    startPoint = temp;
                }

                int count = 1 + endPoint - startPoint;
                Genome.RemoveRange(startPoint, count);

                for (int i = 0; i < count; i++)
                {
                    Genome.Add(StaticFields.RandomByte());
                }
            }
        }

        public NeuralNet GetNueralNetwork()
        {
            return null;
        }

        public override string ToString()
        {
            //return Sensors + ":   " + MarkovNetwork.ToString();
            return Fitness + ":   " + GetMarkovNetwork().ToString();
        }

        public IPhenotype CreateDescendant(double mutationProbability = 0.01, double duplicationProbability = 0.05, double deletionProbability = 0.02, double gateMutationProbability = 0.02)
        {
            PhenotypeMN descendant = new PhenotypeMN(this);
            descendant.MutateNumberOfGates(gateMutationProbability);
            descendant.GeneMutatation(mutationProbability);
            descendant.GeneDuplication(duplicationProbability);
            descendant.GeneDeletion(deletionProbability);
            return descendant;
        }

        public void MutateNumberOfGates(double gateMutationProbability = 0.02)
        {
            if (StaticFields.RandomGenerator.NextDouble() < gateMutationProbability)
            {
                int newGateNumber = StaticFields.RandomGenerator.Next(1, StaticFields._maxNumberOfGates+1);

                if (newGateNumber > _currentNumberOfGates)
                {
                    int size = newGateNumber * (2 /*Nin and Nout*/ + _currentNumberOfInputs /*inputs indexes*/ +
                                                    _currentNumberOfOutputs /*outputindexes*/ +
                                                    (int)Math.Pow(2, _currentNumberOfInputs) * (int)Math.Pow(2,_currentNumberOfOutputs));
                    for (int i = Size; i < size; i++)
                    {
                        Genome.Add(StaticFields.RandomByte());
                    }
                    Size = size;
                }
                else if (newGateNumber < _currentNumberOfGates)
                {
                    Size = newGateNumber * (2 /*Nin and Nout*/ + _currentNumberOfInputs /*inputs indexes*/ +
                                                    _currentNumberOfOutputs /*outputindexes*/ +
                                                    (int)Math.Pow(2, _currentNumberOfInputs) * (int)Math.Pow(2,_currentNumberOfOutputs));
                    Genome = Genome.GetRange(0,Size);
                }
                _currentNumberOfGates = newGateNumber;
            }
        }
    }
}
