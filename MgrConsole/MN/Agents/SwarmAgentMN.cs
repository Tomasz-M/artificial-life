﻿using System;
using System.Collections.Generic;
using MgrConsole.MN.MarkovNetwork;

namespace MgrConsole.MN.Agents
{
    public class SwarmAgentMN : ISwarmAgent
    {
        public int MaxNodes => _numberOfMemoryNodes + _numberOfOutputNodes + _numberOfSensorLayers * _numberOfSensors;
        private int _swarmSize;
        private int _numberOfSensors = StaticFields._numberOfSensors_swarm;
        private int _numberOfSensorLayers = StaticFields._numberOfSensorLayers_swarm;
        private int _numberOfMemoryNodes = StaticFields._numberOfMemoryNodes_swarm;
        private int _numberOfOutputNodes = StaticFields._numberOfOutputNodes_swarm;
        public int _totalSteps = 0;
        public double Fitness { get; set; }
        public double[] Angle { get; set; }
        public double[] PosX { get; set; }
        public double[] PosY { get; set; }

        public int[] States { get; set; }
        private int[] _newStates { get; set; }
        private MarkovNetwork.MN MarkovNetwork { get; set; }

        public SwarmAgentMN(int swarmSize, MarkovNetwork.MN markovNetwork)
        {
            _swarmSize = swarmSize;
            MarkovNetwork = markovNetwork;
            Initialize();
        }

        private void Initialize()
        {
            States = new int[MaxNodes * _swarmSize];
            _newStates = new int[MaxNodes];
            Angle = new double[_swarmSize];
            PosX = new double[_swarmSize];
            PosY = new double[_swarmSize];
            Fitness = 1;
        }

        public void setupMegaPhenotypeMN(int howMany)
        {
            throw new NotImplementedException();
        }

        public void UpdateStates()
        {
            for (int j = 0; j < _swarmSize; j++)
            {
                foreach (Gate gate in MarkovNetwork.Gates)
                {
                    List<int> indexesToSetOn = gate.Compute(States, j * MaxNodes);
                    foreach (int index in indexesToSetOn)
                    {
                        _newStates[index] = 1;
                    }
                }

                for (int i = _numberOfSensorLayers * _numberOfSensors + j * MaxNodes; i < (j + 1) * MaxNodes; i++)
                {
                    States[i] = _newStates[i % MaxNodes];
                }

                for (int i = _numberOfSensorLayers * _numberOfSensors; i < MaxNodes; i++)
                    _newStates[i] = 0;
            }
            _totalSteps++;
        }
        
        public void ClearStates(int i)
        {
            for (int j = 0; j < _numberOfSensors * 2; j++)
            {
                States[j + (i * MaxNodes)] = 0;
            }
        }

        public void ApplyBoundary(double boundaryDist, int i)
        {
            PosX[i] = ApplySingleBoundary(PosX[i], boundaryDist);
            PosY[i] = ApplySingleBoundary(PosY[i], boundaryDist);
        }

        private double ApplySingleBoundary(double positionVal, double boundaryDist)
        {
            double val = positionVal;

            if (Math.Abs(val) > boundaryDist)
            {
                if (val < 0)
                {
                    val = boundaryDist + boundaryDist + val;
                }
                else
                {
                    val = val - boundaryDist - boundaryDist;
                }
            }

            return val;
        }
    }
}
