﻿using System;
using MgrConsole.NeuralNetwork;

namespace MgrConsole.Agents
{
    public class SwarmAgent : ISwarmAgent
    {
        public int MaxNodes => _numberOfMemoryNodes + _numberOfOutputNodes + _numberOfSensorLayers * _numberOfSensors;
        private int _swarmSize;
        private int _numberOfSensors = StaticFields._numberOfSensors_swarm;
        private int _numberOfSensorLayers = StaticFields._numberOfSensorLayers_swarm;
        private int _numberOfMemoryNodes = StaticFields._numberOfMemoryNodes_swarm;
        private int _numberOfOutputNodes = StaticFields._numberOfOutputNodes_swarm;
        public int _totalSteps = 0;
        public double Fitness { get; set; }
        public double[] Angle { get; set; }
        public double[] PosX { get; set; }
        public double[] PosY { get; set; }

        public int[] States { get; set; }
        private int[] _newStates { get; set; }
        private NeuralNet NeuralNetwork { get; set; }

        public SwarmAgent(int swarmSize, NeuralNet net)
        {
            _swarmSize = swarmSize;
            NeuralNetwork = net;
            Initialize();
        }

        private void Initialize()
        {
            States = new int[MaxNodes * _swarmSize];
            _newStates = new int[MaxNodes];
            Angle = new double[_swarmSize];
            PosX = new double[_swarmSize];
            PosY = new double[_swarmSize];
            Fitness = 1; //fix, not 0
        }

        public void setupMegaPhenotype(int howMany)
        {
            //int i, j;
            //tHMMU* hmmu;

            //if (hmmus.size() > 0)
            //{
            //    for (vector<tHMMU*>::iterator it = hmmus.begin(), end = hmmus.end(); it != end; ++it)
            //    {
            //        delete* it;
            //    }
            //}
            //hmmus.clear();
            //for (i = 0; i < genome.size(); i++)
            //{
            //    if ((genome[i] == 42) && (genome[(i + 1) % genome.size()] == (255 - 42)))
            //    {
            //        for (j = 0; j < howMany; j++)
            //        {
            //            hmmu = new tHMMU;
            //            hmmu->setup(genome, i);
            //            //hmmu->setupQuick(genome,i);
            //            for (int k = 0; k < 4; k++)
            //            {
            //                hmmu->ins[k] += (j * maxNodes);
            //                hmmu->outs[k] += (j * maxNodes);
            //            }
            //            hmmus.push_back(hmmu);
            //        }
            //    }
            //    /*
            //     if((genome[i]==43)&&(genome[(i+1)%genome.size()]==(255-43))){
            //     hmmu=new tHMMU;
            //     //hmmu->setup(genome,i);
            //     hmmu->setupQuick(genome,i);
            //     hmmus.push_back(hmmu);
            //     }
            //     */
            //}


            Console.WriteLine("TODO: setupMegaPhenotype");
        }

        public void UpdateStates()
        {
            for (int j = 0; j < _swarmSize; j++)
            {
                int[] newOutputs = NeuralNetwork.SendForward(States, j * MaxNodes);
                int index = 0;
                for (int i = _numberOfSensorLayers * _numberOfSensors + j * MaxNodes; i < (j + 1) * MaxNodes; i++)
                {
                    States[i] = /*(int)*/ newOutputs[index];
                    index++;
                }
            }
        }

        public void ClearStates(int i)
        {
            for (int j = 0; j < _numberOfSensors * 2; j++)
            {
                States[j + (i * MaxNodes)] = 0;
            }
        }

        public void ApplyBoundary(double boundaryDist, int i)
        {
            PosX[i] = ApplySingleBoundary(PosX[i], boundaryDist);
            PosY[i] = ApplySingleBoundary(PosY[i], boundaryDist);
        }

        //private double ApplySingleBoundary(double positionVal, double boundaryDist)
        //{
        //    double val = positionVal;

        //    if (Math.Abs(val) > boundaryDist)
        //    {
        //        if (val < 0)
        //        {
        //            val = -1.0 * boundaryDist;
        //        }
        //        else
        //        {
        //            val = boundaryDist;
        //        }
        //    }

        //    return val;
        //}
        private double ApplySingleBoundary(double positionVal, double boundaryDist)
        {
            double val = positionVal;

            if (Math.Abs(val) > boundaryDist)
            {
                if (val < 0)
                {
                    val = boundaryDist + boundaryDist + val;
                }
                else
                {
                    val = val - boundaryDist - boundaryDist;
                }
            }

            return val;
        }
    }
}
