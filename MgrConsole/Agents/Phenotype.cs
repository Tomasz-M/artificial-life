﻿using System;
using System.Collections.Generic;
using System.Linq;
using MgrConsole.NeuralNetwork;

namespace MgrConsole.Agents
{
    class Phenotype : IPhenotype
    {
        public int Size { get; }
        public int NumberOfOutputNeurons { get; set; }
        public int NumberOfHiddenNeurons { get; set; }
        public int NumberOfSensorsLayers { get; set; }
        public int Sensors { get; set; }
        public int Range { get; set; }

        public List<byte> Genome { get; set; }
        public double Fitness { get; set; }
        public int Id { get; set; }
        public int ParrentId { get; set; }
        public int MasterId { get; set; }
        private int StatesSize { get; set; }

        //numberOfSensorsLayers - 1 for predator, 2 for prey

        public Phenotype(int numberOfOutputs, int numberOfHidden, int sensors = 12, int numberOfSensorsLayers = 1)
        {
            StatesSize = (numberOfSensorsLayers * sensors) + numberOfOutputs + numberOfHidden;
            Size = numberOfHidden * (StatesSize + 1) + numberOfOutputs*(numberOfHidden + 1);
            Range = 180;
            Fitness = 0;
            Sensors = sensors;
            NumberOfSensorsLayers = numberOfSensorsLayers;
            NumberOfOutputNeurons = numberOfOutputs;
            NumberOfHiddenNeurons = numberOfHidden;
            Genome = CreateGenome();
            ParrentId = -1;
            Id = StaticFields._IdCounter++;
            MasterId = Id;
        }

        private Phenotype(Phenotype parent)
        {
            NumberOfSensorsLayers = parent.NumberOfSensorsLayers;
            Sensors = parent.Sensors;
            Range = parent.Range;
            NumberOfOutputNeurons = parent.NumberOfOutputNeurons;
            NumberOfHiddenNeurons = parent.NumberOfHiddenNeurons;
            StatesSize = (NumberOfSensorsLayers * Sensors) + NumberOfHiddenNeurons + NumberOfOutputNeurons;
            Size = NumberOfHiddenNeurons * (StatesSize + 1) + NumberOfOutputNeurons * (NumberOfHiddenNeurons + 1);
            Genome = CopyGenome(parent.Genome);
            ParrentId = parent.Id;
            MasterId = parent.MasterId;
            Id = StaticFields._IdCounter++;
        }

        public Phenotype(string[] strings)
        {
            Id = Convert.ToInt32(strings[1]);
            ParrentId = Convert.ToInt32(strings[2]);
            Fitness = 0;
            NumberOfSensorsLayers = Convert.ToInt32(strings[4]);
            Sensors = Convert.ToInt32(strings[5]);
            Range = Convert.ToInt32(strings[6]);
            NumberOfOutputNeurons = Convert.ToInt32(strings[7]);
            NumberOfHiddenNeurons = Convert.ToInt32(strings[8]);
            StatesSize = (NumberOfSensorsLayers * Sensors) + NumberOfHiddenNeurons + NumberOfOutputNeurons;
            Size = NumberOfHiddenNeurons * (StatesSize + 1) + NumberOfOutputNeurons * (NumberOfHiddenNeurons + 1);

            Genome = new List<byte>();
            int indexOfLastGenome = 10 + Convert.ToInt32(strings[9]);
            for (int i = 10; i < indexOfLastGenome; i++)
            {
                Genome.Add(Convert.ToByte(strings[i]));
            }
            MasterId = Id;
        }

        private List<byte> CopyGenome(List<byte> parentGenome)
        {
            List<byte> newGenome = new List<byte>();
            foreach (byte b in parentGenome)
            {
                newGenome.Add(b);
            }
            return newGenome;
        }

        public IController GetController()
        {
            return GetNueralNetwork();
        }

        public NeuralNet GetNueralNetwork()
        {
            return new NeuralNet(Genome, NumberOfHiddenNeurons, NumberOfOutputNeurons, StatesSize);
        }

        private List<byte> CreateGenome()
        {
            byte[] randomBytes = new byte[Size];
            StaticFields.RandomGenerator.NextBytes(randomBytes);
            return randomBytes.ToList();
        }

        private List<byte> CreateGate()
        {
            byte[] randomBytes = new byte[Size];
            StaticFields.RandomGenerator.NextBytes(randomBytes);
            return randomBytes.ToList();
        }
        
        public void GeneMutatation(double mutationProbability = 0.01)
        {
            for (int i = 0; i < Genome.Count; i++)
            {
                if (StaticFields.RandomGenerator.NextDouble() < mutationProbability)
                    Genome[i] = StaticFields.RandomByte();
            }
        }
        
        public void GeneDuplication(double duplicationProbability = 0.05)
        {
            if (StaticFields.RandomGenerator.NextDouble() < duplicationProbability)
            {
                int startPoint = StaticFields.RandomGenerator.Next(Genome.Count);
                int endPoint = StaticFields.RandomGenerator.Next(Genome.Count);

                if (startPoint > endPoint)
                {
                    int temp = endPoint;
                    endPoint = startPoint;
                    startPoint = temp;
                }

                List<byte> bytesToDuplicate = Genome.GetRange(startPoint, 1 + endPoint - startPoint);
                int startInsertPoint = StaticFields.RandomGenerator.Next(Genome.Count - bytesToDuplicate.Count);
                int currentIndex = 0;

                for (int i = startInsertPoint; i < bytesToDuplicate.Count; i++)
                {
                    Genome[i] = bytesToDuplicate[currentIndex];
                    currentIndex++;
                }
            }
        }

        public void GeneDeletion(double deletionProbability = 0.02)
        {
            if (StaticFields.RandomGenerator.NextDouble() < deletionProbability)
            {
                int startPoint = StaticFields.RandomGenerator.Next(Genome.Count);
                int endPoint = StaticFields.RandomGenerator.Next(Genome.Count);
                
                if (startPoint > endPoint)
                {
                    int temp = endPoint;
                    endPoint = startPoint;
                    startPoint = temp;
                }

                int count = 1 + endPoint - startPoint;
                Genome.RemoveRange(startPoint, count);

                for (int i = 0; i < count; i++)
                {
                    Genome.Add(StaticFields.RandomByte());
                }
            }
        }

        public override string ToString()
        {
            return Fitness + ":   " + GetNueralNetwork().ToString();
        }

        public IPhenotype CreateDescendant(double mutationProbability = 0.01, double duplicationProbability = 0.05, double deletionProbability = 0.02, double gateMutationProbability = 0.02)
        {
            Phenotype descendant = new Phenotype(this);
            descendant.GeneMutatation(mutationProbability);
            descendant.GeneDuplication(duplicationProbability);
            descendant.GeneDeletion(deletionProbability);
            return descendant;
        }
    }
}
