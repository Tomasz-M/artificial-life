﻿using System;
using MgrConsole.NeuralNetwork;

namespace MgrConsole.Agents
{
    class PredatorAgent : IPredatorAgent
    {
        private int _numberOfSensors = StaticFields._numberOfSensors_predator;
        private int _numberOfMemoryNodes = StaticFields._numberOfMemoryNodes_predator;
        private int _numberOfOutputNodes = StaticFields._numberOfOutputNodes_predator;
        public int MaxNodes => _numberOfMemoryNodes + _numberOfOutputNodes + _numberOfSensors;
        public int totalSteps = 0;
        public double Fitness { get; set; }
        public double Angle { get; set; }
        public double PosX { get; set; }
        public double PosY { get; set; }
        public int[] States { get; set; }
        private int[] _newStates;
        private NeuralNet NeuralNetwork { get; set; }

        public PredatorAgent(double gridX, double gridY, NeuralNet net)
        {
            Initialize(gridX, gridY);
            NeuralNetwork = net;
        }

        private void Initialize(double gridX, double gridY)
        {
            States = new int[MaxNodes];
            _newStates = new int[MaxNodes];
            PosX = (StaticFields.RandomGenerator.NextDouble() * gridX * 2.0) - gridX;
            PosY = (StaticFields.RandomGenerator.NextDouble() * gridY * 2.0) - gridY;
            Angle = (int)(StaticFields.RandomGenerator.NextDouble() * 360.0);
            Fitness = 1; //fix, not 0
            totalSteps = 0;
        }
        
        public void setupPhenotype()
        {
            throw new NotImplementedException();
        }

        public void ClearStates()
        {
            for (int i = 0; i < _numberOfSensors; i++)
            {
                States[i] = 0;
            }
        }

        public void UpdateStates()
        {
            int[] newOutputs = NeuralNetwork.SendForward(States);
            int index = 0;
            for (int i = _numberOfSensors; i < MaxNodes; i++)
            {
                States[i] = newOutputs[index];
                index++;
            }
        }
        public void ApplyBoundary(double boundaryDist)
        {
            PosX = ApplySingleBoundary(PosX, boundaryDist);
            PosY = ApplySingleBoundary(PosY, boundaryDist);
        }

        private double ApplySingleBoundary(double positionVal, double boundaryDist)
        {
            double val = positionVal;

            if (Math.Abs(val) > boundaryDist)
            {
                if (val < 0)
                {
                    val = boundaryDist + boundaryDist + val;
                }
                else
                {
                    val = val - boundaryDist - boundaryDist;
                }
            }

            return val;
        }
    }
}
