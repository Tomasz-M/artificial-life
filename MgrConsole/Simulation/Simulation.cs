﻿using MgrConsole.Agents;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Text;
using MgrConsole.MN.Agents;
using MgrConsole.NeuralNetwork;

namespace MgrConsole.Simulation
{
    public class Simulation
    {
        #region Fieds
        //simulation
        private int _totalStepsInSimulation = StaticFields._totalStepsInSimulation;
        private int _numberOfSteps = StaticFields._numberOfSteps;
        private int _populationSize = StaticFields._populationSize;
        private double _boundaryDist = StaticFields._boundaryDist;
        //fields
        double[] _cosLookup = new double[360];
        double[] _sinLookup = new double[360];
        //prey/swarm
        private int _swarmSize = StaticFields._swarmSize;
        private double _safetyDist = StaticFields._safetyDist * StaticFields._safetyDist;
        private int _preySensors = StaticFields._preySensors;
        private double _preyVisionRange = StaticFields._preyVisionRange * StaticFields._preyVisionRange;
        private double _preyVisionAngle = StaticFields._preyVisionAngle / 2;
        private int _preyMaxNodes = StaticFields._preyMaxNodes;
        private double _preyHearingAngle = StaticFields._preyHearingAngle / 2;//FIX potencjalny: /2
        private double _preyHearingRange = StaticFields._preyHearingRange * StaticFields._preyHearingRange;

        //predator
        private int _predatorSensors = StaticFields._predatorSensors;
        private double _gridX = StaticFields._gridX, _gridY = StaticFields._gridY;
        private double _predatorVisionRange = StaticFields._predatorVisionRange * StaticFields._predatorVisionRange;
        private double _predatorVisionAngle = StaticFields._predatorVisionAngle / 2;
        private double _killDist = StaticFields._killDist * StaticFields._killDist;
        private int _killDelay = StaticFields._killDelay;
        private double _confusionMultiplier = StaticFields._confusionMultiplier;
        private int _predatorMaxNodes = StaticFields._predatorMaxNodes;
        //others
        StringBuilder _reportStringB = new StringBuilder();
        List<IPhenotype> _predatorsPhenotypes = new List<IPhenotype>();
        List<IPhenotype> _preyPhenotypes = new List<IPhenotype>();
        private int _predFitness = 0;
        private int _preyFitness = 0;
        //measures
        private int _totalNumAttacks = 0;
        private int _alive = 0;
        private double _currenSumOfDensity = 0;
        private double _bestSumOfDensity = 0;
        private string _currentBestPrey = "";
        private double[] _swarmDensity;
        private double[] _swarmDispersion;
        private int[] _numAlive;
        private int[] _nearbyCount;
        private string alive = "", density = "", dispersion = "", toSave = "";
        private string _saveBaseFileName = "0.csv";
        private string tempHeader;
        #endregion

        #region Running Methods
        private void Initialize()
        {
            if (StaticFields._hearingOn)
            {
                StaticFields._numberOfOutputNodes_swarm = 3;
                StaticFields._numberOfSensorLayers_swarm = 3;
                StaticFields._preyMaxNodes = StaticFields._numberOfSensorLayers_swarm * StaticFields._numberOfSensors_swarm + StaticFields._numberOfMemoryNodes_swarm + StaticFields._numberOfOutputNodes_swarm;
            }
            for (int i = 0; i < 360; ++i)
            {
                _cosLookup[i] = Math.Cos((double)i * (Math.PI / 180.0));
                _sinLookup[i] = Math.Sin((double)i * (Math.PI / 180.0));
            }

            if (StaticFields._readGeneration)
                ReadGeneration(0);
            else
            {
                for (int i = 0; i < _populationSize; i++)
                {
                    _predatorsPhenotypes.Add(new Phenotype(StaticFields._numberOfOutputNodes_predator,
                        StaticFields._numberOfMemoryNodes_predator, _predatorSensors));
                    _preyPhenotypes.Add(new Phenotype(StaticFields._numberOfOutputNodes_swarm,
                        StaticFields._numberOfMemoryNodes_swarm, _preySensors,
                        StaticFields._numberOfSensorLayers_swarm));
                }
            }
            _swarmDensity = new double[_totalStepsInSimulation];
            _swarmDispersion = new double[_totalStepsInSimulation];
            _numAlive = new int[_totalStepsInSimulation];

            SaveCurrentConfiguration();
        }

        private void InitializeFiles()
        {
            StaticFields._saveDataFilePath += Process.GetCurrentProcess().Id + ".";
            StaticFields._generationSavePath += Process.GetCurrentProcess().Id + ".";

            tempHeader = "predId;parPredId;mstPredId;;preyId;parPreyId;mstPreyId;;predFit;preyFit;";
            for (int j = 0; j < 3; j++)
            {
                for (int i = 1; i <= _totalStepsInSimulation; i++)
                {
                    tempHeader += i + ";";
                }
                tempHeader += ";";
                if (j == 0)
                {
                    tempHeader += "numAttacks;;";
                }
            }
            tempHeader += "\n";
            File.WriteAllText(StaticFields._saveDataFilePath + _saveBaseFileName, tempHeader);
            File.WriteAllText(StaticFields._saveDataFilePath + "sum_dat.csv", tempHeader);
            File.WriteAllText(StaticFields._generationSavePath + "best.csv", "");
        }

        private void SaveCurrentConfiguration()
        {
            string configString = "";

            configString += @"#INT" + "\n";
            configString += $"saveDataFilePath={StaticFields._saveDataFilePath}" + "\n";
            configString += $"generationSavePath={StaticFields._generationSavePath}" + "\n";
            configString += $"generationReadFile={StaticFields._generationReadFile}" + "\n";
            configString += $"visualisationSaveFilePath={StaticFields._visualisationSaveFilePath}" + "\n";
            configString += $"generationToSaveCounter={StaticFields._generationToSaveCounter}" + "\n";
            configString += "readGeneration=" + (StaticFields._readGeneration ? 1 : 0) + "\n";
            configString += "saveFrameOn=" + (StaticFields._saveFrameOn ? 1 : 0) + "\n";
            configString += "hearingOn=" + (StaticFields._hearingOn ? 1 : 0) + "\n";
            configString += $"generations={StaticFields._generations}" + "\n";
            configString += $"population={StaticFields._population}" + "\n";
            configString += $"totalStepsInSimulation={StaticFields._totalStepsInSimulation}" + "\n";
            configString += $"numberOfSteps={StaticFields._numberOfSteps}" + "\n";
            configString += $"populationSize={StaticFields._populationSize}" + "\n";
            configString += $"swarmSize={StaticFields._swarmSize}" + "\n";
            configString += $"safetyDist={StaticFields._safetyDist}" + "\n";
            configString += $"preySensors={StaticFields._preySensors}" + "\n";
            configString += $"preyMaxNodes={StaticFields._preyMaxNodes}" + "\n";
            configString += $"predatorSensors={StaticFields._predatorSensors}" + "\n";
            configString += $"killDelay={StaticFields._killDelay}" + "\n";
            configString += $"predatorMaxNodes={StaticFields._predatorMaxNodes}" + "\n";
            configString += $"swarmSize_swarm={StaticFields._swarmSize_swarm}" + "\n";
            configString += $"numberOfSensors_swarm={StaticFields._numberOfSensors_swarm}" + "\n";
            configString += $"numberOfSensorLayers_swarm={StaticFields._numberOfSensorLayers_swarm}" + "\n";
            configString += $"numberOfMemoryNodes_swarm={StaticFields._numberOfMemoryNodes_swarm}" + "\n";
            configString += $"numberOfOutputNodes_swarm={StaticFields._numberOfOutputNodes_swarm}" + "\n";
            configString += $"numberOfSensors_predator={StaticFields._numberOfSensors_predator}" + "\n";
            configString += $"numberOfMemoryNodes_predator={StaticFields._numberOfMemoryNodes_predator}" + "\n";
            configString += $"numberOfOutputNodes_predator={StaticFields._numberOfOutputNodes_predator}" + "\n";
            configString += $"numberOfOutputNeurons={StaticFields._numberOfOutputNeurons}" + "\n";
            configString += $"numberOfHiddenNeurons={StaticFields._numberOfHiddenNeurons}" + "\n";
            configString += $"maxNumberOfGates={StaticFields._maxNumberOfGates}" + "\n";
            configString += $"maxNumberOfInputs={StaticFields._maxNumberOfInputs}" + "\n";
            configString += $"maxNumberOfOutputs={StaticFields._maxNumberOfOutputs}" + "\n";

            configString += "#DOUBLE" + "\n\n";
            configString += $"boundaryDist={StaticFields._boundaryDist}" + "\n";
            configString += $"safetyDist={StaticFields._safetyDist}" + "\n";
            configString += $"preyVisionRange={StaticFields._preyVisionRange}" + "\n";
            configString += $"preyVisionAngle={StaticFields._preyVisionAngle}" + "\n";
            configString += $"preyHearingAngle={StaticFields._preyHearingAngle}" + "\n";
            configString += $"preyHearingRange={StaticFields._preyHearingRange}" + "\n";
            configString += $"grid={StaticFields._gridX}" + "\n";
            configString += $"predatorVisionRange={StaticFields._predatorVisionRange}" + "\n";
            configString += $"predatorVisionAngle={StaticFields._predatorVisionAngle}" + "\n";
            configString += $"killDist={StaticFields._killDist}" + "\n";
            configString += $"confusionMultiplier={StaticFields._confusionMultiplier}" + "\n";
            configString += $"mutationProbability={StaticFields._mutationProbability}" + "\n";
            configString += $"deletionProbability={StaticFields._deletionProbability}" + "\n";
            configString += $"duplicationProbability={StaticFields._duplicationProbability}" + "\n";

            File.WriteAllText(StaticFields._generationSavePath + "config.txt", configString);
        }

        public void RunThread(int populationSize, int generations)
        {
            _populationSize = populationSize;
            Console.WriteLine(_populationSize);
            Initialize();
            InitializeFiles();
            RunSimulation(generations);
        }

        public void RunVisualisation()
        {
            Initialize();
            Console.WriteLine("!");
            Simulate(0, 0);
            Console.WriteLine("Visualisation Simu - DONE");
        }

        private void RunSimulation(int numberOfGenerations)
        {
            SimulatePopulation();
            //SimulatePopulationBestOfThree();
            SaveFile(0);
            TempStatistic(0);

            for (int j = 1; j < numberOfGenerations; j++)
            {
                if (j % StaticFields._generationToSaveCounter == 0)
                {
                    SaveGeneration(j);
                    _saveBaseFileName = j / StaticFields._generationToSaveCounter + ".csv";

                    File.WriteAllText(StaticFields._saveDataFilePath + _saveBaseFileName, tempHeader);
                }

                //SortPopulation();
                SelectNextGeneration();
                //ShufflePopulation();
                SimulatePopulation();
                //SimulatePopulationBestOfThree();
                SaveFile(j);
                TempStatistic(j);
            }
            SaveGeneration(numberOfGenerations);
            Console.WriteLine("DONE");
        }
        private void SimulatePopulationBestOfThree()
        {
            Console.Write("!");
            _nearbyCount = new int[_swarmSize];
            int[,] predFitnesses = new int[_populationSize, 3];
            for (int i = 0; i < _populationSize; i++)
            {
                (string, int, int)[] tuples = new(string, int, int)[3];
                tuples[0] = Simulate(i, i, _numberOfSteps);
                predFitnesses[i, 0] = tuples[0].Item3;
                tuples[1] = Simulate(i, (i + 1) % _populationSize, _numberOfSteps);
                predFitnesses[(i + 1) % _populationSize, 1] = tuples[0].Item3;
                tuples[2] = Simulate(i, (i + 2) % _populationSize, _numberOfSteps);
                predFitnesses[(i + 2) % _populationSize, 2] = tuples[0].Item3;

                int indexOfMedian = GetIndexOfMedian(tuples[0].Item2, tuples[1].Item2, tuples[2].Item2);
                toSave += tuples[indexOfMedian].Item1;
                _preyPhenotypes[i].Fitness = tuples[indexOfMedian].Item2;
            }

            for (int i = 0; i < _populationSize; i++)
            {
                int indexOfMedian = GetIndexOfMedian(predFitnesses[i, 0], predFitnesses[i, 1], predFitnesses[i, 2]);
                _predatorsPhenotypes[i].Fitness = predFitnesses[i, indexOfMedian];
            }
        }

        private int GetIndexOfMedian(int item0, int item1, int item2)
        {
            if (item0 < item1)
            {
                if (item0 >= item2)   //2 0 1
                    return 0;
                if (item1 <= item2)   //0 1 2
                    return 1;
                return 2;             //0 2 1
            }
            else
            {
                if (item0 <= item2)   //1 0 2
                    return 0;
                if (item1 >= item2)   //2 1 0
                    return 1;
                return 2;             //1 2 0
            }
        }

        private void SimulatePopulation()
        {
            Console.Write("!");
            _nearbyCount = new int[_swarmSize];
            for (int i = 0; i < _populationSize; i++)
            {
                (string, int, int)  tuple = Simulate(i, i, _numberOfSteps);
                toSave += tuple.Item1;
                _preyPhenotypes[i].Fitness = tuple.Item2;
                _predatorsPhenotypes[i].Fitness = tuple.Item3;
            }
        }

        private void SaveFile(int generation)
        {
            int div = 1;
            toSave += generation + 1 + "\n";
            File.AppendAllText(StaticFields._saveDataFilePath + _saveBaseFileName, toSave);
            toSave = "";

            toSave += generation + 1 + ";;;;;;;;;;";
            for (int j = 0; j < _totalStepsInSimulation; j++)
            {
                toSave += $"{(double)_numAlive[j] / (div * _populationSize):N2};";
            }
            toSave += ";";
            toSave += $"{(double)_totalNumAttacks / (div * _populationSize):N2};;";
            for (int j = 0; j < _totalStepsInSimulation; j++)
            {
                toSave += $"{_swarmDensity[j] / (div * _populationSize):N2};";
            }
            toSave += ";";
            for (int j = 0; j < _totalStepsInSimulation; j++)
            {
                toSave += $"{_swarmDispersion[j] / (div * _populationSize):N2};";
            }
            toSave += "\n";
            File.AppendAllText(StaticFields._saveDataFilePath + "sum_dat.csv", toSave);
            toSave = "";
        }

        private void TempStatistic(int generation)
        {
            float div = 1f;
            double bestpred = GetBestFitness(_predatorsPhenotypes);
            double bestPrey = GetBestFitness(_preyPhenotypes);
            
            Console.WriteLine(
                "Generation: {0}. PredFit: {1}, PreyFit: {2}, Alive: {3}, MeanAlive: {6}, BestPred: {4}, BestPrey: {5}",
                generation, _predFitness / div, _preyFitness / div, _alive / div, bestpred, bestPrey, _alive / (div * _populationSize)
                /*,_swarmDensity[0], _swarmDensity[_totalStepsInSimulation-1], _swarmDispersion[0], _swarmDispersion[_totalStepsInSimulation - 1]*/);

            _predFitness = 0;
            _preyFitness = 0;
            _alive = 0;
            _totalNumAttacks = 0;
            _swarmDensity = new double[_totalStepsInSimulation];
            _swarmDispersion = new double[_totalStepsInSimulation];
            _numAlive = new int[_totalStepsInSimulation];
        }

        private double GetBestFitness(List<IPhenotype> phenotypes)
        {
            double best = -1;
            foreach (IPhenotype phenotype in phenotypes)
            {
                if (phenotype.Fitness > best)
                    best = phenotype.Fitness;
            }
            return best;
        }
        #endregion

        //(reportString, preyFitness, predatorFitness)
        (string, int, int) Simulate(int predatorsPhenotypeIndex, int preyPhenotypeIndex, int numberOfSteps = 1)
        {
            int swarmFitness = 1;
            int predatorFitness = 1;
            string result = "";
            for (int iii = 0; iii < numberOfSteps; iii++)
            {
                #region Variables
                // counter of how many swarm agents are still alive
                int numAlive = _swarmSize;
                // number of attacks the predator has made
                int numAttacks = 0;

                bool[] preyDead = new bool[_swarmSize];

                // lookup table for distances between predator and swarm agents
                double[] predDistsSqr= new double[_swarmSize];

                // lookup table for distances between swarm agents and other swarm agents
                double[,] preyDistsSqr = new double[_swarmSize, _swarmSize];                
                #endregion

                #region Initialization
                int delay = 0;
                // string containing the information to create a video of the simulation
                string reportString = "";

                // set up brain for clone swarm
                ISwarmAgent swarmAgent = CreateSwarmAgent(preyPhenotypeIndex);

                PlacePrey(swarmAgent, preyDead);

                /*       PRE SIMULATION LOOP OF FREEDOM     */
                int freedomSteps = 250;
                for (int i = 0; i < freedomSteps; i++)
                {
                    if (StaticFields._saveFrameOn)
                        ReportSimulationLoop(swarmAgent, preyDead, delay);

                    _nearbyCount = new int[_swarmSize];
                    RecalcPreyDistTable(swarmAgent.PosX, swarmAgent.PosY, preyDistsSqr);
                    UpdatePrey(swarmAgent, preyDistsSqr);
                    UpdateMeasures(preyDistsSqr, i, preyDead, numAlive);
                }
                /*       END PRE SIMULATION LOOP OF FREEDOM */


                // set up predator brain
                //TODO
                IPredatorAgent predatorAgent = CreatePredatorAgent(predatorsPhenotypeIndex);

                
                // initialize predator and prey lookup tables
                RecalcPredAndPreyDistTable(swarmAgent.PosX, swarmAgent.PosY, preyDead, predatorAgent.PosX, predatorAgent.PosY, predDistsSqr, preyDistsSqr);
                #endregion

                #region Simulation Loop
                /*       BEGINNING OF SIMULATION LOOP       */

                for (int step = freedomSteps; step < _totalStepsInSimulation; ++step)
                {
                    _nearbyCount = new int[_swarmSize];
                    #region SimulationLoop - report
                    /*       CREATE THE REPORT STRING FOR THE VIDEO       */
                    if (StaticFields._saveFrameOn)
                        ReportSimulationLoop(predatorAgent, swarmAgent, preyDead, delay);
                    #region reportcommented
                    //if (report)
                    //{
                    //    // report X, Y, angle of predator
                    //    char text[1000];
                    //    sprintf(text, "%f,%f,%f,%d,%d,%d=", predatorAgent.PosX, predatorAgent.PosY, predatorAgent.Angle, 255, 0, 0);
                    //    reportString.append(text);

                    //    // compute center of swarm
                    //    /*double cX = 0.0, cY = 0.0;
                    //    calcSwarmCenter(swarmAgent.PosX,swarmAgent.PosY, preyDead, cX, cY);

                    //    // report X, Y of center of swarm
                    //    char text2[1000];
                    //    sprintf(text2,"%f,%f,%f,%d,%d,%d=", cX, cY, 0.0, 124, 252, 0);
                    //    reportString.append(text2);*/

                    //    // report X, Y, angle of all prey
                    //    for (int i = 0; i < swarmSize; ++i)
                    //    {
                    //        if (!preyDead[i])
                    //        {
                    //            char text[1000];

                    //            sprintf(text, "%f,%f,%f,%d,%d,%d=", swarmAgent.PosX[i], swarmAgent.PosY[i], swarmAgent.Angle[i], 255, 255, 255);

                    //            reportString.append(text);
                    //        }
                    //    }
                    //    reportString.append("N");

                    //}

                    /*       END OF REPORT STRING CREATION       */
                    #endregion
                    #endregion

                    #region SimulationLoop - UPDATES

                    #region PredatorUpdate

                    UdaptePredator(predatorAgent, preyDead, predDistsSqr, swarmAgent);
                    TryMadeAKill(predatorAgent, preyDead, predDistsSqr, swarmAgent, preyDistsSqr, ref numAlive, ref numAttacks, ref delay);
                    
                    #endregion Predator Update

                    #region Prey Update

                    UpdatePrey(swarmAgent, preyDead, preyDistsSqr, predatorAgent, predDistsSqr);

                    #endregion Prey Update

                    // recalculate both the predator and prey distances lookup tables since the entire swarm has moved
                    RecalcPredAndPreyDistTable(swarmAgent.PosX, swarmAgent.PosY, preyDead, predatorAgent.PosX, predatorAgent.PosY, predDistsSqr, preyDistsSqr);
                    UpdateMeasures(preyDistsSqr, step, preyDead, numAlive);

                    predatorFitness += _swarmSize - numAlive;
                    swarmFitness += numAlive;

                    #endregion SimulationLoop - Updates
                }
                #endregion Simulation Loop

                _predFitness += predatorFitness;
                _preyFitness += swarmFitness;
                _alive += numAlive;
                _totalNumAttacks += numAttacks;
                result = CreateStringToSave(_preyPhenotypes[preyPhenotypeIndex], _predatorsPhenotypes[predatorsPhenotypeIndex], numAttacks, swarmFitness, predatorFitness);
                /*       END OF SIMULATION LOOP       */

                alive = "";
                dispersion = "";
                density = "";
            }
            if (StaticFields._saveFrameOn)
            {
                File.WriteAllText(StaticFields._visualisationSaveFilePath + _preyPhenotypes[preyPhenotypeIndex].Id + ".temptxt",
                    _reportStringB.ToString());
                _reportStringB.Clear();
            }
            if (_currenSumOfDensity > _bestSumOfDensity)
            {
                _bestSumOfDensity = _currenSumOfDensity;
                _currentBestPrey = FormatPhenotypeToSave(_preyPhenotypes[preyPhenotypeIndex]);
            }
            _currenSumOfDensity = 0;

            return (result, swarmFitness, predatorFitness);
        }

        private string CreateStringToSave(IPhenotype preyPhenotype, IPhenotype predatorPhenotype, int numAttacks, int preyFit, int predFit)
        {
            return $"{predatorPhenotype.Id};{predatorPhenotype.ParrentId};{predatorPhenotype.MasterId};;{preyPhenotype.Id};{preyPhenotype.ParrentId};{preyPhenotype.MasterId};;{predFit};{preyFit};"
                   + alive + ";" + numAttacks + ";;" + density + ";" + dispersion + ";" + "\n";
        }

        private void UpdateMeasures(double[,] preyDistsSqr, int step, bool[] preyDead, int numAlive)
        {
            double tempDnes = 0, tepmDisp = 0;
            for (var i = 0; i < _nearbyCount.Length; i++)
            {
                if (!preyDead[i])
                {
                    tempDnes += _nearbyCount[i];
                    tepmDisp += GetNearestDistance(preyDistsSqr, i);
                }
            }
            double temp = tepmDisp / numAlive;
            dispersion += $"{temp:N2};";
            _swarmDispersion[step] += temp;

            temp = tempDnes / numAlive;
            _currenSumOfDensity += tempDnes;
            density += $"{temp:N2};";
            _swarmDensity[step] += temp;

            alive += numAlive + ";";
            _numAlive[step] += numAlive;
        }

        private double GetNearestDistance(double[,] preyDistsSqr, int currentIndex)
        {
            double d = double.MaxValue;
            for (int i = 0; i < _swarmSize; i++)
            {
                if (i==currentIndex)
                    continue;
                if (preyDistsSqr[currentIndex, i] < d)
                    d = preyDistsSqr[currentIndex, i];
            }
            return Math.Sqrt(d);
        }

        #region SimulationMethods
        #region SimulationMethods - Prey
        private ISwarmAgent CreateSwarmAgent(int preyPhenotypeIndex)
        {
            IController controller = _preyPhenotypes[preyPhenotypeIndex].GetController();
            if (controller is NeuralNetwork.NeuralNet)
                return new SwarmAgent(_swarmSize, (NeuralNet)controller);
            else
                return new SwarmAgentMN(_swarmSize, (MN.MarkovNetwork.MN)controller);
        }

        private void PlacePrey(ISwarmAgent swarmAgent, bool[] preyDead)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                int numTries = 0;
                bool goodPos = true;

                do
                {
                    ++numTries;
                    goodPos = true;

                    swarmAgent.PosX[i] = 0.6 * ((double)(StaticFields.RandomGenerator.NextDouble() * _gridX * 2.0) - _gridX);
                    swarmAgent.PosY[i] = 0.6 * ((double)(StaticFields.RandomGenerator.NextDouble() * _gridY * 2.0) - _gridY);

                    for (int j = 0; j < i; ++j)
                    {
                        if (CalcDistanceSquared(swarmAgent.PosX[i], swarmAgent.PosY[i], swarmAgent.PosX[j], swarmAgent.PosY[j]) < _safetyDist)
                        {
                            goodPos = false;
                            break;
                        }
                    }

                } while (!goodPos && numTries < 10);

                swarmAgent.Angle[i] = (int)(StaticFields.RandomGenerator.NextDouble() * 360.0);
                preyDead[i] = false;
            }
        }

        private void UpdatePrey(ISwarmAgent swarmAgent, bool[] preyDead, double[,] preyDists, IPredatorAgent predatorAgent, double[] predDists)
        {
            UpdatePreySensors(swarmAgent, preyDead, preyDists, predatorAgent, predDists);
            // activate the swarm agent's brains
            swarmAgent.UpdateStates();
            // activate each swarm agent's brain, determine its action for this update, and update its position and angle
            MakePreyDecision(swarmAgent, preyDead);
        }

        private void MakePreyDecision(ISwarmAgent swarmAgent, bool[] preyDead)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                if (!preyDead[i])
                {
                    //                                  node 31                                                         node 30
                    int action = ((swarmAgent.States[(swarmAgent.MaxNodes - 1) + (i * swarmAgent.MaxNodes)] & 1) << 1) + (swarmAgent.States[(swarmAgent.MaxNodes - 2) + (i * swarmAgent.MaxNodes)] & 1);

                    switch (action)
                    {
                        // do nothing
                        case 0:
                            break;

                        // turn 8 degrees right
                        case 1:
                            swarmAgent.Angle[i] += 8.0;

                            while (swarmAgent.Angle[i] >= 360.0)
                            {
                                swarmAgent.Angle[i] -= 360.0;
                            }

                            swarmAgent.PosX[i] += _cosLookup[(int)swarmAgent.Angle[i]] * 0.75;
                            swarmAgent.PosY[i] += _sinLookup[(int)swarmAgent.Angle[i]] * 0.75;

                            break;

                        // turn 8 degrees left
                        case 2:
                            swarmAgent.Angle[i] -= 8.0;
                            while (swarmAgent.Angle[i] < 0.0)
                            {
                                swarmAgent.Angle[i] += 360.0;
                            }

                            swarmAgent.PosX[i] += _cosLookup[(int)swarmAgent.Angle[i]] * 0.75;
                            swarmAgent.PosY[i] += _sinLookup[(int)swarmAgent.Angle[i]] * 0.75;

                            break;

                        // move straight ahead
                        case 3:
                            swarmAgent.PosX[i] += _cosLookup[(int)swarmAgent.Angle[i]] * 0.75;
                            swarmAgent.PosY[i] += _sinLookup[(int)swarmAgent.Angle[i]] * 0.75;

                            break;

                        default:
                            break;
                    }

                    // keep position within boundary
                    swarmAgent.ApplyBoundary(_boundaryDist, i);
                }
            }
        }

        private void UpdatePreySensors(ISwarmAgent swarmAgent, bool[] preyDead, double[,] preyDists, IPredatorAgent predatorAgent, double[] predDists)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                if (!preyDead[i])
                {
                    //clear the sensors of agent i
                    swarmAgent.ClearStates(i);

                    // indicate the presence of other visible agents in agent i's retina
                    for (int j = 0; j < _swarmSize; ++j)
                    {
                        //ignore i==j because an agent can't see itself
                        if (i != j && !preyDead[j])
                        {
                            double temp = preyDists[i, j];

                            //don't bother if an agent is too far
                            if (temp < _preyVisionRange)
                            {
                                // ignore if agent i isn't even facing agent j (won't be within retina)
                                if (CalcDistanceSquared(swarmAgent.PosX[i] + _cosLookup[(int)swarmAgent.Angle[i]],
                                        swarmAgent.PosY[i] + _sinLookup[(int)swarmAgent.Angle[i]],
                                        swarmAgent.PosX[j], swarmAgent.PosY[j]) < temp)
                                {
                                    double angle = CalcAngle(swarmAgent.PosX[i], swarmAgent.PosY[i], swarmAgent.Angle[i], swarmAgent.PosX[j], swarmAgent.PosY[j]);

                                    //here we have to map the angle into the sensor, btw: angle in degrees
                                    if (Math.Abs(angle) < _preyVisionAngle) // prey has a limited vision field infront of it
                                    {
                                        swarmAgent.States[(int)(angle / (_preyVisionAngle / ((double)_preySensors / 2.0)) + ((double)_preySensors / 2.0)) + (i * swarmAgent.MaxNodes)] = 1;
                                    }
                                }
                            }
                            //prey i can hear j prey
                            if (StaticFields._hearingOn && temp < _preyHearingRange)
                            {
                                //j prey making a sound
                                if ((swarmAgent.States[(swarmAgent.MaxNodes - 3) + (j * swarmAgent.MaxNodes)]) == 1)
                                {
                                    double angle = CalcAngle(swarmAgent.PosX[i], swarmAgent.PosY[i], swarmAgent.Angle[i],
                                        swarmAgent.PosX[j], swarmAgent.PosY[j]);

                                    ////here we have to map the angle into the sensor, btw: angle in degrees
                                    swarmAgent.States[
                                        _preySensors + _preySensors +
                                        (int)(angle / (_preyVisionAngle / ((double)_preySensors / 2.0)) +
                                              ((double)_preySensors / 2.0)) + (i * swarmAgent.MaxNodes)] = 1;
                                }
                            }
                        }
                    }

                    if (/*d*/predDists[i] < _preyVisionRange)
                    {
                        double angle = CalcAngle(swarmAgent.PosX[i], swarmAgent.PosY[i], swarmAgent.Angle[i], predatorAgent.PosX, predatorAgent.PosY);

                        //here we have to map the angle into the sensor, btw: angle in degree
                        // prey has a limited vision field infront of it
                        if (Math.Abs(angle) < _preyVisionAngle)
                        {
                            swarmAgent.States[_preySensors + (int)(angle / (_preyVisionAngle / ((double)_preySensors / 2.0)) + ((double)_preySensors / 2.0)) + (i * swarmAgent.MaxNodes)] = 1;
                        }
                    }
                }
            }
        }

        #region PreSimulation Update
        private void UpdatePrey(ISwarmAgent swarmAgent, double[,] preyDists)
        {
            UpdatePreySensors(swarmAgent, preyDists);
            // activate the swarm agent's brains
            swarmAgent.UpdateStates();
            // activate each swarm agent's brain, determine its action for this update, and update its position and angle
            MakePreyDecision(swarmAgent);
        }

        private void UpdatePreySensors(ISwarmAgent swarmAgent, double[,] preyDists)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                //clear the sensors of agent i
                swarmAgent.ClearStates(i);

                // indicate the presence of other visible agents in agent i's retina
                for (int j = 0; j < _swarmSize; ++j)
                {
                    //ignore i==j because an agent can't see itself
                    if (i != j)
                    {
                        double temp = preyDists[i, j];

                        //don't bother if an agent is too far
                        if (temp < _preyVisionRange)
                        {
                            // ignore if agent i isn't even facing agent j (won't be within retina)
                            if (CalcDistanceSquared(swarmAgent.PosX[i] + _cosLookup[(int)swarmAgent.Angle[i]],
                                    swarmAgent.PosY[i] + _sinLookup[(int)swarmAgent.Angle[i]],
                                    swarmAgent.PosX[j], swarmAgent.PosY[j]) < temp)
                            {
                                double angle = CalcAngle(swarmAgent.PosX[i], swarmAgent.PosY[i], swarmAgent.Angle[i],
                                    swarmAgent.PosX[j], swarmAgent.PosY[j]);

                                //here we have to map the angle into the sensor, btw: angle in degrees
                                if (Math.Abs(angle) < _preyVisionAngle) // prey has a limited vision field infront of it
                                {
                                    swarmAgent.States[
                                        (int)(angle / (_preyVisionAngle / ((double)_preySensors / 2.0)) +
                                              ((double)_preySensors / 2.0)) + (i * swarmAgent.MaxNodes)] = 1;
                                }
                            }
                        }
                        //prey i can hear j prey
                        if (StaticFields._hearingOn && temp < _preyHearingRange)
                        {
                            //j prey making a sound
                            if ((swarmAgent.States[(swarmAgent.MaxNodes - 3) + (j * swarmAgent.MaxNodes)]) == 1)
                            {
                                double angle = CalcAngle(swarmAgent.PosX[i], swarmAgent.PosY[i], swarmAgent.Angle[i],
                                    swarmAgent.PosX[j], swarmAgent.PosY[j]);

                                ////here we have to map the angle into the sensor, btw: angle in degrees
                                swarmAgent.States[
                                    _preySensors + _preySensors +
                                    (int)(angle / (_preyVisionAngle / ((double)_preySensors / 2.0)) +
                                          ((double)_preySensors / 2.0)) + (i * swarmAgent.MaxNodes)] = 1;
                            }
                        }
                    }
                }
            }
        }

        private void MakePreyDecision(ISwarmAgent swarmAgent)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                //                                  node 31                                                         node 30
                int action = ((swarmAgent.States[(swarmAgent.MaxNodes - 1) + (i * swarmAgent.MaxNodes)] & 1) << 1) + (swarmAgent.States[(swarmAgent.MaxNodes - 2) + (i * swarmAgent.MaxNodes)] & 1);

                switch (action)
                {
                    // do nothing
                    case 0:
                        break;

                    // turn 8 degrees right
                    case 1:
                        swarmAgent.Angle[i] += 8.0;

                        while (swarmAgent.Angle[i] >= 360.0)
                        {
                            swarmAgent.Angle[i] -= 360.0;
                        }

                        swarmAgent.PosX[i] += _cosLookup[(int)swarmAgent.Angle[i]] * 0.75;
                        swarmAgent.PosY[i] += _sinLookup[(int)swarmAgent.Angle[i]] * 0.75;

                        break;

                    // turn 8 degrees left
                    case 2:
                        swarmAgent.Angle[i] -= 8.0;
                        while (swarmAgent.Angle[i] < 0.0)
                        {
                            swarmAgent.Angle[i] += 360.0;
                        }

                        swarmAgent.PosX[i] += _cosLookup[(int)swarmAgent.Angle[i]] * 0.75;
                        swarmAgent.PosY[i] += _sinLookup[(int)swarmAgent.Angle[i]] * 0.75;

                        break;

                    // move straight ahead
                    case 3:
                        swarmAgent.PosX[i] += _cosLookup[(int)swarmAgent.Angle[i]] * 0.75;
                        swarmAgent.PosY[i] += _sinLookup[(int)swarmAgent.Angle[i]] * 0.75;

                        break;

                    default:
                        break;
                }

                // keep position within boundary
                swarmAgent.ApplyBoundary(_boundaryDist, i);
            }
        }

        private void RecalcPreyDistTable(double[] preyX, double[] preyY, double[,] preyDists)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                preyDists[i, i] = 0.0;

                for (int j = i + 1; j < _swarmSize; ++j)
                {
                    double distsquared = CalcDistanceSquared(preyX[i], preyY[i], preyX[j], preyY[j]);
                    preyDists[i, j] = distsquared;
                    preyDists[j, i] = distsquared;
                    //swarm denstity
                    if (distsquared < _safetyDist) //30*30
                    {
                        _nearbyCount[i]++;
                        _nearbyCount[j]++;
                    }
                }
            }
        }
        #endregion PreSimulation Update

        #endregion SimulationMethods - Prey
        #region SimulationMethods - Predator
        private IPredatorAgent CreatePredatorAgent(int predatorsPhenotypeIndex)
        {
            IController controller = _predatorsPhenotypes[predatorsPhenotypeIndex].GetController();
            if (controller is NeuralNetwork.NeuralNet)
                return new PredatorAgent(_gridX, _gridY, (NeuralNet)controller);
            else
                return new PredatorAgentMN(_gridX, _gridY, (MN.MarkovNetwork.MN)controller);
        }

        private void TryMadeAKill(IPredatorAgent predatorAgent, bool[] preyDead, double[] predDists, ISwarmAgent swarmAgent, double[,] preyDists, ref int numAlive, ref int numAttacks, ref int delay)
        {
            // determine if the predator made a kill
            if (numAlive > 2)
            {
                if (delay < 1)
                {
                    bool killed = false;

                    for (int i = 0; !killed && i < _swarmSize; ++i)
                    {
                        // victim prey must be within kill range
                        if (!preyDead[i] && (predDists[i] < _killDist) &&
                            Math.Abs(CalcAngle(predatorAgent.PosX, predatorAgent.PosY, predatorAgent.Angle,
                                swarmAgent.PosX[i], swarmAgent.PosY[i])) < _predatorVisionAngle)
                        {
                            ++numAttacks;
                            int nearbyCount = 0;

                            for (int j = 0; j < _swarmSize; ++j)
                            {
                                // other prey must be close to target prey and within predator's retina
                                if (!preyDead[j] && preyDists[i, j] < _safetyDist &&
                                    predDists[j] < _predatorVisionRange &&
                                    Math.Abs(CalcAngle(predatorAgent.PosX, predatorAgent.PosY, predatorAgent.Angle,
                                        swarmAgent.PosX[j], swarmAgent.PosY[j])) <
                                    _predatorVisionAngle)
                                {
                                    ++nearbyCount;
                                }
                            }

                            // confusion effect
                            double killChance = _confusionMultiplier/(double) nearbyCount;

                            if (StaticFields.RandomGenerator.NextDouble() < killChance)
                            {
                            killed = true;
                                preyDead[i] = killed;
                                --numAlive;
                            }

                            // add a short delay in between kill attempts
                            delay = _killDelay;
                            break;
                        }
                    }
                }
                else
                {
                    --delay;
                }
            }

        }

        private void UdaptePredator(IPredatorAgent predatorAgent, bool[] preyDead, double[] predDists, ISwarmAgent swarmAgent)
        {

            // clear the predator sensors
            predatorAgent.ClearStates();

            UpdatePredatorSensors(preyDead, predatorAgent, predDists, swarmAgent);

            // activate the predator agent's brain
            predatorAgent.UpdateStates();

            // make decision for predator
            MakePredDecision(predatorAgent);

            // keep position within simulation boundary
            predatorAgent.ApplyBoundary(_boundaryDist);

            // recalculate the predator distances lookup table since the predator has moved
            RecalcPredDistTable(swarmAgent.PosX, swarmAgent.PosY, preyDead, predatorAgent.PosX, predatorAgent.PosY, predDists);

        }

        private void MakePredDecision(IPredatorAgent predatorAgent)
        {
            //                                      node 31                                              node 30
            int action = ((predatorAgent.States[(predatorAgent.MaxNodes - 1)] & 1) << 1) +
                         (predatorAgent.States[(predatorAgent.MaxNodes - 2)] & 1);
            switch (action)
            {
                // do nothing
                case 0:
                    break;

                // turn right
                case 1:
                    predatorAgent.Angle += 6.0;

                    while (predatorAgent.Angle >= 360.0)
                    {
                        predatorAgent.Angle -= 360.0;
                    }

                    predatorAgent.PosX += _cosLookup[(int)predatorAgent.Angle] * 2.25;
                    predatorAgent.PosY += _sinLookup[(int)predatorAgent.Angle] * 2.25;

                    break;

                // turn left
                case 2:
                    predatorAgent.Angle -= 6.0;

                    while (predatorAgent.Angle < 0.0)
                    {
                        predatorAgent.Angle += 360.0;
                    }

                    predatorAgent.PosX += _cosLookup[(int)predatorAgent.Angle] * 2.25;
                    predatorAgent.PosY += _sinLookup[(int)predatorAgent.Angle] * 2.25;

                    break;

                // move straight ahead
                case 3:
                    predatorAgent.PosX += _cosLookup[(int)predatorAgent.Angle] * 2.25;
                    predatorAgent.PosY += _sinLookup[(int)predatorAgent.Angle] * 2.25;

                    break;

                default:
                    break;
            }
        }

        private void UpdatePredatorSensors(bool[] preyDead, IPredatorAgent predatorAgent, double[] predDists, ISwarmAgent swarmAgent)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                if (!preyDead[i])
                {
                    // don't bother if an agent is too far
                    if (predDists[i] < _predatorVisionRange)
                    {
                        double angle = CalcAngle(predatorAgent.PosX, predatorAgent.PosY, predatorAgent.Angle, swarmAgent.PosX[i], swarmAgent.PosY[i]);

                        // here we have to map the angle into the sensor, btw: angle in degrees
                        if (Math.Abs(angle) < _predatorVisionAngle) // predator has a limited vision field in front of it
                        {
                            predatorAgent.States[(int)(angle / (_predatorVisionAngle / ((double)_predatorSensors / 2.0)) + ((double)_predatorSensors / 2.0))] = 1;
                        }
                    }
                }
            }
        }

        #endregion SimulationMethods - Predator
        #endregion SimulationMethods

        #region Statistics and reports
        private void ReportSimulationLoop(IPredatorAgent predatorAgent, ISwarmAgent swarmAgent, bool[] preyDead, int delay)
        {
            _reportStringB.Append(1 + ";" + predatorAgent.PosX + ";" + predatorAgent.PosY + ";" + predatorAgent.Angle + ";");
            for (int i = 0; i < _swarmSize; i++)
            {
                _reportStringB.Append(preyDead[i] ? "0" : "1" + ";" + swarmAgent.PosX[i] + ";" + swarmAgent.PosY[i] + ";" + swarmAgent.Angle[i] + ";");
            }
            _reportStringB.Append(delay == 0 ? "1" : "0");
            _reportStringB.AppendLine();
        }
        private void ReportSimulationLoop(ISwarmAgent swarmAgent, bool[] preyDead, int delay)
        {
            _reportStringB.Append(1 + ";" + 3*_gridX + ";" + 3 * _gridX + ";" + 0 + ";");
            for (int i = 0; i < _swarmSize; i++)
            {
                _reportStringB.Append(preyDead[i] ? "0" : "1" + ";" + swarmAgent.PosX[i] + ";" + swarmAgent.PosY[i] + ";" + swarmAgent.Angle[i] + ";");
            }
            _reportStringB.Append(delay == 0 ? "1" : "0");
            _reportStringB.AppendLine();
        }

        private void CalculateSwarmDensity()
        {
            
        }
        #endregion Statistics and reports
        
        #region EnviromentalCalculations
        private void RecalcPredDistTable(double[] preyX, double[] preyY, bool[] preyDead,
        double predX, double predY,
        double[] predDists)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                if (!preyDead[i])
                {
                    predDists[i] = CalcDistanceSquared(predX, predY, preyX[i], preyY[i]);
                }
            }
        }

        private double CalcAngle(double fromX, double fromY, double fromAngle, double toX, double toY)
        {
            double Ux = 0.0, Uy = 0.0, Vx = 0.0, Vy = 0.0;

            if (fromX - toX > _gridX)
                fromX -= 2 * _gridX;
            else if (toX - fromX > _gridX)
                fromX += 2 * _gridX;
            if (fromY - toY > _gridY)
                fromY -= 2 * _gridY;
            else if (toY - fromY > _gridY)
                fromY += 2 * _gridY;

            Ux = (toX - fromX);
            Uy = (toY - fromY);

            Vx = _cosLookup[(int)fromAngle];
            Vy = _sinLookup[(int)fromAngle];

            int firstTerm = (int)((Ux * Vy) - (Uy * Vx));
            int secondTerm = (int)((Ux * Vx) + (Uy * Vy));

            return Math.Atan2(firstTerm, secondTerm) * 180.0 / Math.PI;
        }

        private void RecalcPredAndPreyDistTable(double[] preyX, double[] preyY, bool[] preyDead, double predX, double predY, double[] predDists, double[,] preyDists)
        {
            for (int i = 0; i < _swarmSize; ++i)
            {
                if (!preyDead[i])
                {
                    predDists[i] = CalcDistanceSquared(predX, predY, preyX[i], preyY[i]);
                    preyDists[i, i] = 0.0;

                    for (int j = i + 1; j < _swarmSize; ++j)
                    {
                        if (!preyDead[j])
                        {
                            double distsquared = CalcDistanceSquared(preyX[i], preyY[i], preyX[j], preyY[j]);
                            preyDists[i, j] = distsquared;
                            preyDists[j, i] = distsquared;
                            //swarm denstity
                            if (distsquared < 900) //30*30
                            {
                                _nearbyCount[i]++;
                                _nearbyCount[j]++;
                            }
                        }
                    }
                }
            }
        }

        private double CalcDistanceSquared(double fromX, double fromY, double toX, double toY)
        {
            double temp = fromX - toX;
            double diffX = Math.Min(temp, (2 * _gridX) - temp);
            temp = fromY - toY;
            double diffY = Math.Min(temp, (2 * _gridY) - temp);

            return (diffX * diffX) + (diffY * diffY);
        }
        #endregion

        #region GA
        private void ShufflePopulation()
        {
            _predatorsPhenotypes = _predatorsPhenotypes.OrderBy(p => StaticFields.RandomGenerator.Next()).ToList();
            _preyPhenotypes = _preyPhenotypes.OrderBy(p => StaticFields.RandomGenerator.Next()).ToList();
        }

        private void SelectNextGeneration()
        {
            _preyPhenotypes = CreateNewPopulation(SelextParentsForNextPopulation(_preyPhenotypes, _populationSize));
        }

        private List<IPhenotype> SelextParentsForNextPopulation(List<IPhenotype> currentPopulation, int _populationSize)
        {
            currentPopulation = currentPopulation.OrderByDescending(p => p.Fitness).ToList();
            return currentPopulation.GetRange(0, _populationSize / 2);
        }

        private List<IPhenotype> CreateNewPopulation(List<IPhenotype> phenotypes)
        {
            List<IPhenotype> newPopulation = new List<IPhenotype>();
            foreach (IPhenotype parent in phenotypes)
            {
                newPopulation.Add(parent.CreateDescendant(StaticFields._mutationProbability, StaticFields._duplicationProbability, StaticFields._deletionProbability));
                newPopulation.Add(parent.CreateDescendant(StaticFields._mutationProbability, StaticFields._duplicationProbability, StaticFields._deletionProbability));
                //newPopulation.Add(parent);//if parrent included
            }
            return newPopulation;
        }

        private int GetIndexOfNextPhenotypeToAdd(double prob, double[] probabilities)
        {
            for (int i = 0; i < probabilities.Length; i++)
            {
                if (prob < probabilities[i])
                    return i;
            }
            return probabilities.Length;
        }
        #endregion GA

        #region saving
        private void SaveBestAfterGeneration(int generation)
        {
            File.AppendAllText(StaticFields._generationSavePath + "best.csv", _currentBestPrey);
        }

        private string FormatPhenotypeToSave(IPhenotype phenotype, bool isPray = true)
        {
            if (phenotype is Phenotype)
                return FormatPhenotypeToSaveNN((Phenotype)phenotype, isPray);
            else
                return FormatPhenotypeToSaveMN((PhenotypeMN)phenotype, isPray);

        }

        private string FormatPhenotypeToSaveNN(Phenotype phenotype, bool isPray = true)
        {
            int size = phenotype.Genome.Count;
            string result = isPray ? "1" : "0";
            result += String.Format(";{0};{1};{2};{3};{4};{5};{6};{7};{8}", phenotype.Id, phenotype.ParrentId, phenotype.Fitness,
                phenotype.NumberOfSensorsLayers, phenotype.Sensors, phenotype.Range,
                phenotype.NumberOfOutputNeurons, phenotype.NumberOfHiddenNeurons, size);//TODO
            for (int i = 0; i < size; i++)
            {
                result += ";" + phenotype.Genome[i];
            }
            result += ";" + phenotype.MasterId;//NEW
            result += "\n";
            return result;
        }
        private string FormatPhenotypeToSaveMN(PhenotypeMN phenotype, bool isPray = true)
        {
            int size = phenotype.Genome.Count;
            string result = isPray ? "1" : "0";
            result += String.Format(";{0};{1};{2};{3};{4};{5};{6}", phenotype.Id, phenotype.ParrentId,
                phenotype.MaxNodes, phenotype.NumberOfSensorsLayers, phenotype.Sensors, phenotype.Range,
                size);
            for (int i = 0; i < size; i++)
            {
                result += ";" + phenotype.Genome[i];
            }
            result += ";" + phenotype._currentNumberOfGates;//NEW
            result += ";" + phenotype.MasterId;//NEW
            result += "\n";
            return result;
        }
        private void SaveGeneration(int generation)
        {
            string generationSave = "";
            foreach (IPhenotype phenotype in _predatorsPhenotypes)
                generationSave += FormatPhenotypeToSave(phenotype, false);

            foreach (IPhenotype phenotype in _preyPhenotypes)
                generationSave += FormatPhenotypeToSave(phenotype);

            File.WriteAllText(StaticFields._generationSavePath + generation + ".csv", generationSave);

            SaveBestAfterGeneration(generation);
        }
        
        private void ReadGeneration(int generation)
        {
            StreamReader file = new StreamReader(StaticFields._generationReadFile);
            bool preyIsNN = false, predIsNN = false;
            try
            {
                string line = file.ReadLine();
                string[] currentLine = line.Split(';');
                {
                    predIsNN = currentLine[0] == "0";
                    preyIsNN = currentLine[1] == "0";
                }

                while ((line = file.ReadLine()) != null)
                {
                    currentLine = line.Split(';');
                    if (currentLine[0] == "0")
                    {
                        IPhenotype phenotype;
                        if (predIsNN)
                        {
                            phenotype = new Phenotype(currentLine);

                        }
                        else
                        {
                            phenotype = new PhenotypeMN(currentLine);
                        }

                        _predatorsPhenotypes.Add(phenotype);
                    }

                    else
                    {
                        IPhenotype phenotype;
                        if (preyIsNN)
                        {
                            phenotype = new Phenotype(currentLine);

                        }
                        else
                        {
                            phenotype = new PhenotypeMN(currentLine);
                        }

                        _preyPhenotypes.Add(phenotype);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                if (_predatorsPhenotypes.Count > _preyPhenotypes.Count)
                    for (int i = _preyPhenotypes.Count; i < _predatorsPhenotypes.Count; i++)
                    {
                        if (preyIsNN)
                        {
                            _preyPhenotypes.Add(new Phenotype(StaticFields._numberOfOutputNodes_swarm,
                                StaticFields._numberOfMemoryNodes_swarm, _preySensors,
                                StaticFields._numberOfSensorLayers_swarm));
                        }
                        else
                        {
                            _preyPhenotypes.Add(new PhenotypeMN(StaticFields._preyMaxNodes, StaticFields._numberOfSensorLayers_swarm));
                        }
                    }
                if (_predatorsPhenotypes.Count < _preyPhenotypes.Count)
                {
                    for (int i = _predatorsPhenotypes.Count; i < _preyPhenotypes.Count; i++)
                    {
                        if (predIsNN)
                        {
                            _predatorsPhenotypes.Add(new Phenotype(StaticFields._numberOfOutputNodes_predator,
                                StaticFields._numberOfMemoryNodes_predator, _predatorSensors));
                        }
                        else
                        {
                            _predatorsPhenotypes.Add(new PhenotypeMN(StaticFields._predatorMaxNodes)); //if pred is MN
                        }
                    }
                }

                file.Close();
            }
        }
        #endregion
    }
}
