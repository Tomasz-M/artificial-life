﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgrConsole
{
    interface IPredatorAgent
    {
        int MaxNodes { get; }
        double Fitness { get; set; }
        double Angle { get; set; }
        double PosX { get; set; }
        double PosY { get; set; }

        int[] States { get; set; }
        void ClearStates();
        void UpdateStates();
        void ApplyBoundary(double boundaryDist);
    }
}
