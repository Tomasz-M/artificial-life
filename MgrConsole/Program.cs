﻿using System;
using System.IO;

namespace MgrConsole
{
    class Program
    {
        #region Fields
        private static string _configPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + "\\config.txt";
        #endregion
        static void Main(string[] args)
        {
            ReadConfiguration();
            Simulation.Simulation s = new Simulation.Simulation();

            if (StaticFields._saveFrameOn)
                s.RunVisualisation();
            else
                s.RunThread(StaticFields._populationSize, StaticFields._generations);

            Console.ReadKey();
        }

        private static void ReadConfiguration()
        {
            StreamReader file = new StreamReader(_configPath);
            try
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    string[] currentLine = line.Split('=');
                    if (currentLine.Length < 2)
                        continue;
                    switch (currentLine[0])
                    {
                        case "#":
                            break;

                        #region simulation
                        case "saveDataFilePath":
                            StaticFields._saveDataFilePath = currentLine[1];
                            break;
                        case "generationSavePath":
                            StaticFields._generationSavePath = currentLine[1];
                            break;
                        case "generationReadFile":
                            StaticFields._generationReadFile = currentLine[1];
                            break;
                        case "visualisationSaveFilePath":
                            StaticFields._visualisationSaveFilePath = currentLine[1];
                            break;
                        case "generationToSaveCounter":
                            StaticFields._generationToSaveCounter = Int32.Parse(currentLine[1]);
                            break;
                        case "saveFrameOn":
                            StaticFields._saveFrameOn = Int32.Parse(currentLine[1]) == 1;
                            break;
                        case "hearingOn":
                            StaticFields._hearingOn = Int32.Parse(currentLine[1]) == 1;
                            break;
                        case "readGeneration":
                            StaticFields._readGeneration = Int32.Parse(currentLine[1]) == 1;
                            break;
                        case "generations":
                            StaticFields._generations = Int32.Parse(currentLine[1]);
                            break;
                        case "population":
                            StaticFields._populationSize = Int32.Parse(currentLine[1]);
                            break;
                        case "totalStepsInSimulation":
                            StaticFields._totalStepsInSimulation = Int32.Parse(currentLine[1]);
                            break;
                        case "numberOfSteps":
                            StaticFields._numberOfSteps = Int32.Parse(currentLine[1]);
                            break;
                        case "populationSize":
                            StaticFields._populationSize = Int32.Parse(currentLine[1]);
                            break;
                        case "boundaryDist":
                            StaticFields._boundaryDist = Double.Parse(currentLine[1]);
                            break;
                        case "swarmSize":
                            StaticFields._swarmSize = Int32.Parse(currentLine[1]);
                            break;
                        case "safetyDist":
                            StaticFields._safetyDist = Double.Parse(currentLine[1]);
                            break;
                        case "preySensors":
                            StaticFields._preySensors = Int32.Parse(currentLine[1]);
                            break;
                        case "preyVisionRange":
                            StaticFields._preyVisionRange = Double.Parse(currentLine[1]);
                            break;
                        case "preyVisionAngle":
                            StaticFields._preyVisionAngle = Double.Parse(currentLine[1]);
                            break;
                        case "preyHearingRange":
                            StaticFields._preyHearingRange = Double.Parse(currentLine[1]);
                            break;
                        case "preyHearingAngle":
                            StaticFields._preyHearingAngle = Double.Parse(currentLine[1]);
                            break;
                        case "preyMaxNodes":
                            StaticFields._preyMaxNodes = Int32.Parse(currentLine[1]);
                            break;
                        case "predatorSensors":
                            StaticFields._predatorSensors = Int32.Parse(currentLine[1]);
                            break;
                        case "grid":
                            StaticFields._gridX = Double.Parse(currentLine[1]);
                            StaticFields._gridY = Double.Parse(currentLine[1]);
                            break;
                        case "predatorVisionRange":
                            StaticFields._predatorVisionRange = Double.Parse(currentLine[1]);
                            break;
                        case "predatorVisionAngle":
                            StaticFields._predatorVisionAngle = Double.Parse(currentLine[1]);
                            break;
                        case "killDist":
                            StaticFields._killDist = Double.Parse(currentLine[1]);
                            break;
                        case "killDelay":
                            StaticFields._killDelay = Int32.Parse(currentLine[1]);
                            break;
                        case "confusionMultiplier":
                            StaticFields._confusionMultiplier = Double.Parse(currentLine[1]);
                            break;
                        case "predatorMaxNodes":
                            StaticFields._predatorMaxNodes = Int32.Parse(currentLine[1]);
                            break;
                        #endregion
                        #region Swarm
                        case "swarmSize_swarm":
                            StaticFields._swarmSize_swarm = Int32.Parse(currentLine[1]);
                            break;
                        case "numberOfSensors_swarm":
                            StaticFields._numberOfSensors_swarm = Int32.Parse(currentLine[1]);
                            break;
                        case "numberOfSensorLayers_swarm":
                            StaticFields._numberOfSensorLayers_swarm = Int32.Parse(currentLine[1]);
                            break;
                        case "numberOfMemoryNodes_swarm":
                            StaticFields._killDist = Int32.Parse(currentLine[1]);
                            break;
                        case "numberOfOutputNodes_swarm":
                            StaticFields._killDist = Int32.Parse(currentLine[1]);
                            break;
                        #endregion
                        #region Predator
                        case "numberOfSensors_predator":
                            StaticFields._numberOfSensors_predator = Int32.Parse(currentLine[1]);
                            break;
                        case "numberOfMemoryNodes_predator":
                            StaticFields._numberOfMemoryNodes_predator = Int32.Parse(currentLine[1]);
                            break;
                        case "numberOfOutputNodes_predator":
                            StaticFields._numberOfOutputNodes_predator = Int32.Parse(currentLine[1]);
                            break;
                        #endregion
                        #region phenotype
                        case "numberOfOutputNeurons":
                            StaticFields._numberOfOutputNeurons = Int32.Parse(currentLine[1]);
                            break;
                        case "numberOfHiddenNeurons":
                            StaticFields._numberOfHiddenNeurons = Int32.Parse(currentLine[1]);
                            break;
                        case "mutationProbability":
                            StaticFields._mutationProbability = Double.Parse(currentLine[1]);
                            break;
                        case "deletionProbability":
                            StaticFields._deletionProbability = Double.Parse(currentLine[1]);
                            break;
                        case "duplicationProbability":
                            StaticFields._duplicationProbability = Double.Parse(currentLine[1]);
                            break;
                        #endregion
                        #region Others
                        case "maxNumberOfGates":
                            StaticFields._maxNumberOfGates = Int32.Parse(currentLine[1]);
                            break;
                        case "maxNumberOfInputs":
                            StaticFields._maxNumberOfInputs = Int32.Parse(currentLine[1]);
                            break;
                        case "maxNumberOfOutputs":
                            StaticFields._maxNumberOfOutputs = Int32.Parse(currentLine[1]);
                            break;
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                file.Close();
            }
        }
    }
}
