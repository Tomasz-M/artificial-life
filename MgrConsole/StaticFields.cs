﻿using System;

namespace MgrConsole
{
    public static class StaticFields
    {
        public static bool _saveFrameOn = false;
        public static bool _hearingOn = false;
        public static Random RandomGenerator = new Random();

        public static int _maxNumberOfGates = 4;
        public static int _maxNumberOfInputs = 4;
        public static int _maxNumberOfOutputs = 4;
        public static int _IdCounter = 0; 
        public static string _saveDataFilePath = @"D:\Nowy folder\praca naukowa\stats\";
        public static string _generationSavePath = @"D:\Nowy folder\praca naukowa\generations\";
        public static string _generationReadFile = @"D:\Nowy folder\praca naukowa\generations\generationTest.csv";
        public static string _visualisationSaveFilePath = @"D:\Nowy folder\praca naukowa\visual\";
        public static int _generationToSaveCounter = 100;
        public static bool _readGeneration = false;

        #region Simulation
        //simulation
        public static int _totalStepsInSimulation = 2000;
        public static int _numberOfSteps = 1;
        public static int _populationSize = 100;
        public static double _boundaryDist = 256;
        public static int _generations = 1000;
        public static double _population = 50;
        //prey/swarm
        public static int _swarmSize = 50;
        public static double _safetyDist = 30;
        public static int _preySensors = 12;
        public static double _preyVisionRange = 100;
        public static double _preyVisionAngle = 180;
        public static int _preyMaxNodes = 32;
        public static double _preyHearingAngle = 360;
        public static double _preyHearingRange = 100;
        //predator
        public static int _predatorSensors = 12;
        public static double _gridX = 256.0, _gridY = 256.0;
        public static double _predatorVisionRange = 200;
        public static double _predatorVisionAngle = 180;
        public static double _killDist = 5;
        public static int _killDelay = 10;
        public static double _confusionMultiplier = 1;
        public static int _predatorMaxNodes = 20;
        #endregion

        #region SwarmAgent
        public static int _swarmSize_swarm = 50;
        public static int _numberOfSensors_swarm = 12;
        public static int _numberOfSensorLayers_swarm = 2;
        public static int _numberOfMemoryNodes_swarm = 6;
        public static int _numberOfOutputNodes_swarm = 2;
        #endregion

        #region PredatorAgent
        public static int _numberOfSensors_predator = 12;
        public static int _numberOfMemoryNodes_predator = 6;
        public static int _numberOfOutputNodes_predator = 2;
        #endregion

        #region phenotype
        public static int _numberOfOutputNeurons;
        public static int _numberOfHiddenNeurons;
        public static double _mutationProbability = 0.01;
        public static double _deletionProbability = 0.02;
        public static double _duplicationProbability = 0.05;
        #endregion

        public static byte RandomByte()
        {
            return (byte)RandomGenerator.Next(256);
        }
    }
}
