﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgrConsole.NeuralNetwork;

namespace MgrConsole
{
    public interface IPhenotype
    {
        int Size { get; }
        int NumberOfSensorsLayers { get; set; }
        int Sensors { get; set; }
        int Range { get; set; }

        List<byte> Genome { get; set; }
        double Fitness { get; set; }
        int Id { get; set; }
        int ParrentId { get; set; }
        int MasterId { get; set; }

        void GeneMutatation(double mutationProbability = 0.01);
        void GeneDuplication(double duplicationProbability = 0.05);
        void GeneDeletion(double deletionProbability = 0.02);

        IController GetController();

        IPhenotype CreateDescendant(double mutationProbability = 0.01, double duplicationProbability = 0.05, double deletionProbability = 0.02, double gateMutationProbability = 0.02);
    }
}
